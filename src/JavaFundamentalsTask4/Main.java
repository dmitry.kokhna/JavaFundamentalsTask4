package JavaFundamentalsTask4;

import java.lang.reflect.Array;
import java.util.Scanner;

//   Ввести целые числа как аргументы командной строки, подсчитать их сумму (произведение) и вывести результат на консоль.
public class Main {

    public static void main(String[] args) {
        // сделаем с воодом через консоль
        // и сделаем с переменными с присвоением
        System.out.println("Введите 4-ре целых чисела");
        Scanner inputDigit=new Scanner(System.in);

        int[] arrayOfDigitsForSum=new int[4];

        for (int i=0; i<arrayOfDigitsForSum.length;i++){
            arrayOfDigitsForSum[i]=inputDigit.nextInt();
        }


        //подсчет суммы чисел массива
        int sumOfDigits=0;
        for (int j=0;j<arrayOfDigitsForSum.length;j++) {
           sumOfDigits=sumOfDigits+arrayOfDigitsForSum[j];
                    }
        System.out.println("Сумма чисел " + sumOfDigits);

        //подсчет произведение чисел массива
        int multiplicationOfDigits=1;
        for (int k=0;k<arrayOfDigitsForSum.length;k++) {
            multiplicationOfDigits=multiplicationOfDigits*arrayOfDigitsForSum[k];
        }
        System.out.println("Произведение чисел " + multiplicationOfDigits);
    }


}